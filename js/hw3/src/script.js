const style = ["Джаз", "Блюз"];

document.write("Начальное содержимое массива: " + style );
document.write("<br>");
document.write("<hr>");

style.push("Рок-н-ролл");
document.write("Массив после добавления в конец нового элемента:  " + style);


style[parseInt(style.length/2)] = "Классика";
document.write("<br>");
document.write("<hr>");
document.write("Массив с заменой среднего элемента: " + style);

document.write("<br>");
document.write("<hr>");
document.write("Удаляем и выводим первый элемент массива: " +style.shift());

document.write("<br>");
document.write("<hr>");
style.unshift("Рэп","Регги");
document.write("Добавляем 2 элемента в начало массива: " + style);
document.write("<hr>");
