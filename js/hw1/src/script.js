var x=6;
var y=14;
var z=4;
//сначала работает умножение, причем x не меняет значение, ибо постфиксная форма возвращает //
//старое значение. потом вычетание и результат вычетания добавляется к заявленному значению х//
x+=y-x++*z;
document.write("result x+:" + x + '</br>');

//сначала происходит умножение, х становится меньше на единицу(префиксная форма)//
//от нового значения х вычетаем результат умножения//
x=6;
y=14;
z=4;
z=--x-y*5;
document.write("result z:" + z + '</br>');

//находим остаток от деления, добавляем его к х. у делим на полученное число//
x=6;
y=14;
z=4;
y/=x+5%z;
document.write("result y/:" + y + '</br>');


//приоритет ++у, умножаем на 5 и добавляем к х//
x=6;
y=14;
z=4;
z=x++ +y*5;
document.write("result z:" + z + '</br>');

//х не меняется, проходит умножение и от у отнимаем результат умножения//
x=6;
y=14;
z=4;
x=y-x++*z;
document.write("result z:" + x + '</br>');
